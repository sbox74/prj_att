#!/bin/sh

# deployment host address
ip_addr=$1
# версия сборки
tag_version=$CI_COMMIT_TAG

chmod 600 $SSH_USER_KEY

ssh -i $SSH_USER_KEY -o 'UserKnownHostsFile=/dev/null' -o StrictHostKeyChecking=no \
    "sysadmin@$ip_addr" "cd ~/inno_att/ansible; ansible-playbook update_landscape.yaml --extra-vars app1_ver=$tag_version --extra-vars app2_ver=$tag_version --extra-vars haproxy_ver=$tag_version --extra-vars nginx_ver=$tag_version"
