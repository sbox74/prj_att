#!/bin/sh

# deployment host address
ip_addr=$1
# action: create, drop, start, stop
action=$2

chmod 600 $SSH_USER_KEY

ssh -i $SSH_USER_KEY -o 'UserKnownHostsFile=/dev/null' -o StrictHostKeyChecking=no \
    "sysadmin@$ip_addr" "cd ~/inno_att/ansible; ansible-playbook ${action}_landscape.yaml"
